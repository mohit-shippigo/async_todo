const winston = require("winston");
const bodyParser = require("body-parser");
const express = require("express");
const mongoose = require('mongoose');
const app = express();

const config = require('./config/config.json')
const logger = require('./config/logger')
const mongoConnectionString = config.mongo_URI
const port = process.env.PORT || config.PORT
const secret = config.SECRET

app.set("secretKey", secret)

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

mongoose.connect(mongoConnectionString, { useUnifiedTopology: true, useNewUrlParser: true }, (err, connect) => {
    if (err) logger.log("error", `=-=-=-=mongoDb err-=-=-=>, ${err}`)
    else logger.log("info", 'Connected to Database')
})


const routes = require('./routes/routes')
app.use('/', routes)

app.listen(port, () => logger.log("info", `${__filename}    Server started, Port :  ${port}`));