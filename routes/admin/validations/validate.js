const login = require('./login');
const logout = require('./logout');

module.exports = (req, res, next) => {
    const url = req.originalUrl.split('/');
    const finalUrl = url[url.length - 1]
    switch (finalUrl) {

        /**
         * Login
         */
        case 'login':
            login(req, res, next)
            break;

            /**
             * Logout
             */
        case 'logout':
            logout(req, res, next)
            break;


        default:
            next();
    }

}