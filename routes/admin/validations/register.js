const regex = require('../../common/regex');

module.exports = (req, res, next) => {

    if (!req.body.firstName || !req.body.lastName || !req.body.email || !req.body.password) {
        res.json({
            success: false,
            msg: "Please enter all details."
        })
    } else {
        req.body.firstName = req.body.firstName.toLowerCase().trim()
        req.body.lastName = req.body.lastName.toLowerCase().trim()
        req.body.email = req.body.email.trim()
        req.body.password = req.body.password.trim()

        let firstNameCheck = regex.regexAlphabets.test(req.body.firstName)
        let lastNameCheck = regex.regexAlphabets.test(req.body.lastName)
        let emailCheck = regex.emailRegex.test(req.body.email)
        let passwordCheck = regex.passwordRegex.test(req.body.password)

        if (!firstNameCheck || !lastNameCheck || !emailCheck || !passwordCheck) {
            res.json({
                success: false,
                msg: "Entered format is invalid."
            })
        } else {
            next();
        }
    }
}