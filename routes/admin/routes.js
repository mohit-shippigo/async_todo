const express = require('express');
const validate = require('./validations/validate');
const tokenVerify = require('../common/tokenVerify');
const router = express.Router();


/****************************** Admin Registration ******************************/
router.post('/register', validate, require('./register'))


/****************************** Admin Login ******************************/
router.post('/login', validate, require('./login').login)


/****************************** Admin Logout ******************************/
router.post('/logout', validate, tokenVerify, require('./login').logout)


module.exports = router