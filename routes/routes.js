const express = require('express');
const router = express.Router();



/************************** Admin **************************/
router.use('/admin', require('./admin/routes'));


/************************** User **************************/
router.use('/user', require('./user/routes'));


// /************************** Task **************************/
// router.use('/movie', require('./movie/routes'));


/************************** Universal Error Handler **************************/
router.use((err, req, res, next) => {
    // Log the errors to a file
    logger.error(`${new Date().toISOString()} | ${req.method} - ${req.originalUrl} | IP: ${req.ip} | Error: ${err.message} | Body: ${JSON.stringify(req.body)} | Headers: ${JSON.stringify(req.headers)}`);
    // Send a response
    res.json({
        success: false,
        msg: "Something went wrong, please try again later."
    })
})




module.exports = router;