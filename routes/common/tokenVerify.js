const jwt = require('jsonwebtoken');
const dbUserLogin = require('../../models/registration')

module.exports = (req, res, next) => {

    let token = req.headers['x-access-token'];

    if (token) {
        jwt.verify(token, req.app.get('secretKey'), (err, decoded) => {
            if (err) {
                res.json({
                    success: false,
                    msg: "Failed to authenicate token."
                })
            } else {
                if (decoded.user === 'user') {
                    dbUserLogin.findOne({ email: decoded.email, phone: decoded.phone }, (err, login) => {
                        if (err || login == null) {
                            res.json({
                                success: false,
                                msg: "Something went wrong"
                            })
                        } else if (login.token && login.token == req.headers['x-access-token']) {
                            req.decoded = decoded;
                            next();
                        } else {
                            res.json({
                                success: false,
                                msg: "unauthorized token"
                            })
                        }
                    })
                } else if (decoded.user === 'admin') {
                    req.decoded = decoded;
                    next();
                } else if (!decoded.user || decoded.user == null) {
                    req.decoded = decoded;
                    next();
                } else {
                    res.json({
                        success: false,
                        msg: "Unauthorized token."
                    })
                }

            }
        })
    } else {
        res.json({
            success: false,
            msg: "No token found."
        })
    }
}