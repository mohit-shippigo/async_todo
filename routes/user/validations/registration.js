const regex = require('../../common/regex');

module.exports = (req, res, next) => {

    if (!req.body.firstName || !req.body.lastName || !req.body.email || !req.body.phone || !req.body.password) {
        res.json({
            success: false,
            msg: "Please fill all details"
        })
    } else {
        req.body.firstName = req.body.firstName.toLowerCase().trim()
        req.body.lastName = req.body.lastName.toLowerCase().trim()
        req.body.email = req.body.email.toLowerCase().trim()
        req.body.phone = req.body.phone.trim()
        req.body.password = req.body.password.trim()

        let emailCheck = regex.emailRegex.test(req.body.email)
        let firstNameCheck = regex.regexAlphabets.test(req.body.firstName)
        let lastNameCheck = regex.regexAlphabets.test(req.body.lastName)
        let phoneCheck = regex.phoneRegex.test(req.body.phone)
        let passwordCheck = regex.passwordRegex.test(req.body.password)

        if (!emailCheck || !firstNameCheck || !lastNameCheck || !phoneCheck || !passwordCheck) {
            res.json({
                success: false,
                msg: "Entered format is invalid."
            })
        } else {
            next();
        }
    }
}