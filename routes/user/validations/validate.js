const register = require('./registration')
const login = require('./login')
const logout = require('./logout')

module.exports = (req, res, next) => {
    const url = req.originalUrl.split('/');
    const finalUrl = url[url.length - 1];

    switch (finalUrl) {

        /**
         * Register User
         */
        case 'register':
            register(req, res, next)
            break;


            /**
             * User Login
             */
        case 'login':
            login(req, res, next)
            break;


            /**
             * User logout
             */
        case 'logout':
            logout(req, res, next)
            break;


        default:
            next();
    }

}