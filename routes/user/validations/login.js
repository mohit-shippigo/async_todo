const regex = require('../../common/regex');

module.exports = (req, res, next) => {

    if (!req.body.email || !req.body.password) {
        res.json({
            success: false,
            msg: "Please enter all details."
        })
    } else {
        req.body.email = req.body.email.toLowerCase().trim()
        req.body.password = req.body.password.trim()

        let emailCheck = regex.emailRegex.test(req.body.email)

        if (!emailCheck) {
            res.json({
                success: false,
                msg: "Entered format is invalid."
            })
        } else {
            next();
        }
    }
}