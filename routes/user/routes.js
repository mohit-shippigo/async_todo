const express = require('express');
const router = express.Router();
const tokenVerify = require('../common/tokenVerify');
const validate = require('./validations/validate')


/************* Registered User **************/
router.post('/register', validate, require('./registration'))


/************* Login User **************/
router.post('/login', validate, require('./login').login)


/************* Logout User **************/
router.post('/logout', validate, tokenVerify, require('./login').logout)


module.exports = router