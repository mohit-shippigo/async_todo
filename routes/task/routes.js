const tokenVerify = require('../common/tokenVerify');
const validate = require('./validations/validate')
const express = require('express');
const router = express.Router();


/************************** Add Task ************************/
router.post('/assignTask', tokenVerify, validate, require('./assignTask'));


/************************** Completed Task ************************/
router.post('/completedTask', tokenVerify, validate, require('./completedTask'));


/************************** Delete Task ************************/
router.post('/deleteTask', tokenVerify, validate, require('./deleteTask'));


/************************** Search Task ************************/
router.post('/searchTask', tokenVerify, validate, require('./searchTask'));


/************************** Show All Task ************************/
router.post('/showAllTask', tokenVerify, validate, require('./showAllTask'));


/************************** UpDate Task ************************/
router.post('/updateTask', tokenVerify, validate, require('./updateTask'));


module.exports = router;