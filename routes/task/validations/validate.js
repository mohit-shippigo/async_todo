const assignTask = require('./assignTask');
const deleteTask = require('./deleteTask');
const searchTask = require('./searchTask');
const updateTask = require('./updateTask');

module.exports = (req, res, next) => {
    const url = req.originalUrl.split('/');
    const finalUrl = url[url.length - 1]
    switch (finalUrl) {

        /**
         * Add Task
         */
        case 'assignTask':
            assignTask(req, res, next)
            break;

            /**
             * Delete Task
             */
        case 'deleteTask':
            deleteTask(req, res, next)
            break;

            /**
             * Search Task
             */
        case 'searchTask':
            searchTask(req, res, next)
            break;

            /**
             * update Task
             */
        case 'updateTask':
            updateTask(req, res, next)
            break;


        default:
            next();
    }

}