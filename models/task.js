const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const taskSchema = new Schema({
    taskId: String,
    taskName: {
        type: String,
        required: true,
    },
    description: String,
    assignedTo: [{
        type: String,
        required: true,
    }],
    isCompleted: {
        type: Boolean,
        default: false
    },
    createdAt: { type: new Date }
});



module.exports = mongoose.model('user', taskSchema)