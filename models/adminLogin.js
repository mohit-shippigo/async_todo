const mongoose = require('mongoose')
const Schema = mongoose.Schema;

var adminLogin = new Schema({
    email: String,
    password: String,
    firstName: String,
    lastName: String,
    token: String
})

module.exports = mongoose.model('admin', adminLogin)