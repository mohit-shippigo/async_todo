const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const userSchema = new Schema({
    firstName: {
        type: String,
        required: true,
    },
    lastName: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    phone: {
        type: Number,
        required: true,
        unique: true
    },
    username: String,
    password: {
        type: String,
        required: true
    },
    token: String,
    lastLogin: [Date],
    createdAt: { type: Date, default: Date.now },
});



module.exports = mongoose.model('user', userSchema)